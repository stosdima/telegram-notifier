<?php

class Telegram_Bot
{
    private $offset;
    private $options;
    private $api;
    private $db;

    public function __construct()
    {
        $this->options = get_option('telegram_bot_options');
        $this->api = new TelegramBot\Api\BotApi($this->options['bot_token']);
        $this->db = new Telegram_Db();
        add_action('draft_to_publish', [$this, 'send_post_to_telegram_users']);
        if ($_SERVER["SERVER_ADDR"] == '127.0.0.1' || !is_ssl()) {
            add_action('init', [$this, 'long_poll_chat_commands_responce']);
        } else {
            add_action('init', [$this, 'setWebhook']);
        }
    }

    public function send_post_to_telegram_users()
    {
        $recent_post = wp_get_recent_posts(['numberposts' => 1]);
        foreach ($this->db->chatAll() as $id) {
            foreach ($recent_post as $post) {
                $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
                    [
                        [
                            ['text' => 'Show at the site', 'url' => get_permalink($post['ID'])]
                        ]
                    ]
                );
                $text = $this->generate_telegram_post(get_permalink($post['ID']), $post['post_title'],
                    $post['post_content']);
                $this->api->sendMessage($id->chat_id, $text, 'html', false, null, $keyboard);
            }
        }
    }

    private function generate_telegram_post($postUrl, $postTitle, $postBody)
    {
        return '<a href=' . '"' . $postUrl . '"' . '>' . $postTitle . '</a>' . strip_tags("\n" . substr("$postBody", 0,
                    400));
    }

    public function long_poll_chat_commands_responce()
    {
        try {
            $this->offset = 0;
            $response = $this->api->getUpdates($this->offset, 60);
            foreach ($response as $data) {
                if ($data->getMessage()) {
                    $chatId = $data->getMessage()->getChat()->getId();
                    $firstName = $data->getMessage()->getChat()->getFirstName();
                    $lastName = $data->getMessage()->getChat()->getLastName();
                    $status = $this->db->getStatus($chatId);
                    switch ($data->getMessage()->getText()) {
                        case '/start':
                            $this->db->addContact($chatId);
                            $text = 'Hello, thank`s for subscribing. Commands list: /help';
                            $this->db->resetStatus($chatId);
                            $this->api->sendMessage($chatId, $text, null, false, null, null);
                            break;
                        case '/stop':
                            $this->db->deleteContact($chatId);
                            $text = 'You have been deleted from bot database. If you want start again, please, send me /start';
                            $this->api->sendMessage($chatId, $text);
                            break;
                        case '/search':
                            $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
                                [
                                    [
                                        ['text' => 'Categories', 'callback_data' => 'categories'],
                                        ['text' => 'Keyword', 'callback_data' => 'search-keyword'],
                                    ]
                                ]
                            );
                            $this->db->updateStatus($chatId, 'search-keyword');
                            $text = 'Search by: ';
                            $this->api->sendMessage($chatId, $text, 'html', false, null, $keyboard);
                            break;
                        case '/admin':
                            if (!$this->db->isAdmin($chatId)) {
                                $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
                                    [
                                        [
                                            ['text' => 'Login', 'callback_data' => 'login'],
                                        ]
                                    ]
                                );
                            } else {
                                $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
                                    [
                                        [
                                            ['text' => 'Create Post', 'callback_data' => 'post-create'],
                                            ['text' => 'Delete Post', 'callback_data' => 'post-delete'],
                                            ['text' => 'User statistic', 'callback_data' => 'statistic'],
                                        ]
                                    ]
                                );
                            }
                            $this->db->updateStatus($chatId, 'admin');
                            $text = 'Welcome, ' . $firstName . ' ' . $lastName . '  you are in admin panel.';
                            $this->api->sendMessage($chatId, $text, 'html', false,
                                null,
                                $keyboard);
                            break;
                        case '/help':
                            $text =
                                'List of commands:
                        /start - start work with bot 
                        /stop - stop work with bot
                        /search - search posts by categories
                        /admin - site administrator panel
                        <b>if you want get quote input random message</b>';
                            $this->api->sendMessage($chatId, $text, 'html');
                            break;
                        default:
                            if ($status[0]->status == 'start') {
                                $text = '<b>I`m not chat bot. You can read some quote:)</b>' . "<i>{$this->get_quote()}</i>";
                                $this->api->sendMessage($chatId, $text, 'html');
                            }
                            break;
                    }
                    if ($status) {
                        switch ($status[0]->status) {
                            case 'admin-verif':
                                if ($this->options['verif_code'] == $data->getMessage()->getText()) {
                                    $text = 'Yo are logged in. Thanks!';
                                    $this->api->sendMessage($chatId, $text);
                                    $this->db->updateAdmin($chatId);
                                    $this->db->resetStatus($chatId);
                                } else {
                                    $text = 'Incorrect verification code. Please re-type: ';
                                    $this->api->sendMessage($chatId, $text);
                                }
                                break;
                            case 'admin-post':
                                $postContent = explode('::', $data->getMessage()->getText());
                                if (count($postContent) == 2) {
                                    $postData = [
                                        'post_status' => 'publish',
                                        'post_author' => 1,
                                        'post_title' => $postContent[0],
                                        'post_content' => $postContent[1],
                                    ];
                                    $text = 'You are awesome! <b>Post was created</b>';
                                    $this->api->sendMessage($chatId, $text, 'html');
                                    $newPost = wp_insert_post($postData);
                                    foreach ($this->db->chatAll() as $id) {
                                        $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
                                            [
                                                [
                                                    ['text' => 'Show at the site', 'url' => get_permalink($newPost)]
                                                ]
                                            ]
                                        );
                                        $this->db->updateStatus($chatId, 'start');
                                        $text = $this->generate_telegram_post(get_permalink($newPost),
                                            $postData['post_title'], $postData['post_content']);
                                        $this->api->sendMessage($id->chat_id, $text, 'html', false, null, $keyboard);
                                    }
                                } else {
                                    $text = 'Incorrect delimiter, please re-type <b>data( example - TITLE :: BODY)</b>';
                                    $this->api->sendMessage($chatId, $text, 'html');
                                }
                                break;
                            case 'admin-post-delete':
                                if (wp_delete_post($data->getMessage()->getText())) {
                                    $text = 'Post was deleted.';
                                    $this->api->sendMessage($chatId, $text);
                                    $this->db->resetStatus($chatId);
                                } else {
                                    $text = 'Error. Please re-type Post ID:';
                                    $this->api->sendMessage($chatId, $text);
                                }
                                break;
                            case 'search-keyword':
                                $posts = $this->db->searchByKeyword($data->getMessage()->getText());
                                if ($posts) {
                                    $text = '';
                                    foreach ($posts as $post) {
                                        $text .= $this->generate_telegram_post(get_permalink($post->ID),
                                                $post->post_title, $post->post_content) . "\n";
                                    }
                                    $this->api->sendMessage($chatId, $text, 'html', false, null);
                                    $this->db->resetStatus($chatId);
                                } else {
                                    $text = 'The search did not give a result.';
                                    $this->api->sendMessage($chatId, $text);
                                    $this->db->resetStatus($chatId);
                                }
                                break;
                        }
                    }
                }
                if ($data->getCallbackQuery()) {
                    $callbackId = $data->getCallbackQuery()->getMessage()->getChat()->getId();
                    switch ($data->getCallbackQuery()->getData()) {
                        case 'categories':
                            if (get_categories()) {
                                $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup([]);
                                foreach (get_categories() as $category) {
                                    $keyboard->setInlineKeyboard([
                                        [
                                            ['text' => $category->name, 'callback_data' => $category->term_id]
                                        ]
                                    ]);
                                }
                                $text = 'List of categories: ';
                                $this->db->updateStatus($callbackId, 'search-categories');
                                $this->api->sendMessage($callbackId, $text, null, false, null, $keyboard);
                            } else {
                                $text = 'At that moment you haven`t created categories ';
                                $this->api->sendMessage($callbackId, $text, null, false, null, null);
                                $this->db->resetStatus($callbackId);
                            }
                            break;
                        case 'login':
                            $text = 'Insert <b>verification code</b> from your site: ';
                            $this->api->sendMessage($callbackId, $text, 'html');
                            $this->db->updateStatus($callbackId, 'admin-verif');
                            break;
                        case 'post-create':
                            $text = 'Send me, please, your post <b>data( example - TITLE :: BODY)</b>: ';
                            $this->db->updateStatus($callbackId, 'admin-post');
                            $this->api->sendMessage($callbackId, $text, 'html');
                            break;
                        case 'search-keyword':
                            $text = 'Please, type <b>keyword</b> and you will get list of posts:';
                            $this->db->updateStatus($callbackId, 'search-keyword');
                            $this->api->sendMessage($callbackId, $text, 'html');
                            break;
                        case 'post-delete':
                            $posts = get_posts(['numberposts' => 0]);
                            if ($posts) {
                                $text = 'Please choose post ID which you want to delete from list below: ' . "\n";
                                foreach ($posts as $post) {
                                    $text .= $this->generate_telegram_post(get_permalink($post->ID), $post->post_title,
                                            'ID -> ' . $post->ID) . "\n";
                                }
                                $this->db->updateStatus($callbackId, 'admin-post-delete');
                                $this->api->sendMessage($callbackId, $text, 'html');
                            } else {
                                $text = 'You havent created posts :(';
                                $this->db->resetStatus($callbackId);
                                $this->api->sendMessage($callbackId, $text);
                            }
                            break;
                        case 'statistic':
                            $allusers = count($this->db->chatAll());
                            $text = 'Current users: ' . $allusers;
                            $this->db->resetStatus($callbackId);
                            $this->api->sendMessage($callbackId, $text);
                            break;
                    }
                    foreach (get_categories() as $category) {
                        if ($data->getCallbackQuery()->getData() == $category->term_id) {
                            $posts = get_posts(['category' => $category->term_id]);
                            $text = '';
                            if ($posts) {
                                foreach ($posts as $post) {
                                    $text .= $this->generate_telegram_post(get_permalink($post->ID), $post->post_title,
                                            $post->post_content) . "\n";
                                }
                                $this->api->sendMessage($callbackId, $text, 'html');
                                $this->db->resetStatus($callbackId);
                            } else {
                                $text = 'There are no posts in this category';
                                $this->api->sendMessage($callbackId, $text);
                            }
                        }
                    }
                }
                $this->offset = $response[count($response) - 1]->getUpdateId() + 1;
            }
            $response = $this->api->getUpdates($this->offset, 60);
        } catch (\TelegramBot\Api\Exception $e) {
            /*echo $e->getMessage();*/
        }
    }

    public function setWebhook()
    {
        if (isset($_REQUEST['page']) && $_REQUEST['page'] == 'telegram-settings') {
            $pluginUrl = plugins_url('telegram-notifier/telegram-notifier.php');
            try {
                $this->api->setWebhook($pluginUrl);
            } catch (\TelegramBot\Api\Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    public function webhook_chat_command_responce()
    {
        try {
            $bot = new \TelegramBot\Api\Client($this->options['bot_token']);
            $bot->command('start', function ($message) use ($bot) {
                $bot->sendMessage($message->getChat()->getId(),
                    'Hello,' . $message->getChat()->getFirstName() . ', thank`s for subscribing. Commands list: /help');
            });
            $bot->command('help', function ($message) use ($bot) {
                $commandList = 'List of commands:
                         /start - start work with bot
                         /stop - stop work with bot
                         /search - search posts by categories
                         /admin - site administrator panel
                         if you want get quote input random message';
                $bot->sendMessage($message->getChat()->getId(), $commandList);
            });
            $bot->command('search', function ($message) use ($bot) {
                $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
                    [
                        [
                            ['text' => 'Categories', 'callback_data' => 'categories'],
                            ['text' => 'Keyword', 'callback_data' => 'keyword']
                        ]
                    ]
                );
                $bot->sendMessage($message->getChat()->getId(), 'Search by:', null, false, null, $keyboard);
            });
            $bot->command('admin', function ($message) use ($bot) {
                /*Need verification if logged user*/
                $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
                    [
                        [
                            ['text' => 'Login', 'callback_data' => 'login'],
                            ['text' => 'Create post', 'callback_data' => 'create-post'],
                            ['text' => 'Delete post', 'callback_data' => 'delete-post'],
                        ]
                    ]
                );
                /*End verification if logged user*/
                $bot->sendMessage($message->getChat()->getId(), 'Hello, you are in admin panel!', null, false, null,
                    $keyboard);
            });
            $bot->callbackQuery(function (\TelegramBot\Api\Types\CallbackQuery $callbackQuery) use ($bot) {
                if ($callbackQuery->getData() == 'categories') {
                    /*WORDPRESS CODE...*/
                    $bot->sendMessage($callbackQuery->getFrom()->getId(), 'List of categories:');
                } elseif ($callbackQuery->getData() == 'keyword') {
                    /*WORDPRESS CODE...*/
                    $bot->sendMessage($callbackQuery->getFrom()->getId(), 'Searching by keywords');
                } elseif ($callbackQuery->getData() == 'login') {
                    /*WORDPRESS CODE...*/
                    $bot->sendMessage($callbackQuery->getFrom()->getId(), 'Login functionality');
                } elseif ($callbackQuery->getData() == 'create-post') {
                    /*WORDPRESS CODE...*/
                    $bot->sendMessage($callbackQuery->getFrom()->getId(), 'Post created');
                } elseif ($callbackQuery->getData() == 'delete-post') {
                    /*WORDPRESS CODE...*/
                    $bot->sendMessage($callbackQuery->getFrom()->getId(), 'Post deleted');
                }
            });
            $bot->run();
        } catch (\TelegramBot\Api\Exception $e) {
            $e->getMessage();
        }
    }

    private function get_quote()
    {
        $quotes = "Don't cry because it's over, smile because it happened. ― Dr. Seuss
        Two things are infinite: the universe and human stupidity; and I'm not sure about the universe. ― Albert Einstein
        Be who you are and say what you feel, because those who mind don't matter, and those who matter don't mind ― Bernard M. Baruch
        A room without books is like a body without a soul. ― Marcus Tullius Cicero
        You know you're in love when you can't fall asleep because reality is finally better than your dreams. ― Dr. Seuss
        You only live once, but if you do it right, once is enough. ― Mae West
        Be the change that you wish to see in the world. ― Mahatma Gandhi
        In three words I can sum up everything I've learned about life: it goes on. ― Robert Frost
        If you tell the truth, you don't have to remember anything. ― Mark Twain
        A friend is someone who knows all about you and still loves you. ― Elbert Hubbard
        Always forgive your enemies; nothing annoys them so much. ― Oscar Wilde
        Live as if you were to die tomorrow. Learn as if you were to live forever. ― Mahatma Gandhi
        I am so clever that sometimes I don't understand a single word of what I am saying. ― Oscar Wilde, The Happy Prince and Other Stories
        Without music, life would be a mistake. ― Friedrich Nietzsche, Twilight of the Idols";
        $quotes = explode("\n", $quotes);
        return $quotes[mt_rand(0, count($quotes) - 1)];
    }
}